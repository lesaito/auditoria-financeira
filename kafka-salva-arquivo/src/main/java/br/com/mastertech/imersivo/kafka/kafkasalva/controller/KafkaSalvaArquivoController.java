package br.com.mastertech.imersivo.kafka.kafkasalva.controller;

import br.com.mastertech.imersivo.kafka.model.DadosCNPJ;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Component
public class KafkaSalvaArquivoController {

    private static final String NOME_ARQUIVO = "DadosCNPJ.csv";

    @KafkaListener(topics = "leandro-biro-2", groupId = "leandro-biro-2")
    public void recebeDadosCNPJ(@Payload DadosCNPJ dadosCNPJ) {
        System.out.println("Recebi dados do CNPJ: " + dadosCNPJ.getCnpj());

        escreveArquivoAcessos(NOME_ARQUIVO, dadosCNPJ);
    }


    private void escreveArquivoAcessos(String arquivo, DadosCNPJ dadosCNPJ) {
        try {
            String registro = String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", dadosCNPJ.getNome(), dadosCNPJ.getTelefone(), dadosCNPJ.getSituacao(),
                    dadosCNPJ.getAbertura(), dadosCNPJ.getNaturezaJuridica(), dadosCNPJ.getCnpj(), dadosCNPJ.getStatus(), dadosCNPJ.getTipo(),
                    dadosCNPJ.getCapitalSocial(), dadosCNPJ.getStatusAnalise());
            Files.write(Paths.get(arquivo), registro.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}