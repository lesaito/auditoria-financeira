package br.com.mastertech.imersivo.kafka.kafkaregister.service;

import br.com.mastertech.imersivo.kafka.model.CNPJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaRegisterService {

    @Autowired//       (tipo chave, tipo valor)
    private KafkaTemplate<String, CNPJ> sender;

    public void enviarCNPJ(CNPJ cnpj) {
        //         (topico, chave, valor)
        sender.send("leandro-biro-1", "1", cnpj);
    }
}
