package br.com.mastertech.imersivo.kafka.kafkaregister.controller;

import br.com.mastertech.imersivo.kafka.kafkaregister.service.KafkaRegisterService;
import br.com.mastertech.imersivo.kafka.model.CNPJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaRegisterController {

    @Autowired
    public KafkaRegisterService registerService;

    @PostMapping("/registro")
    private void registraAcesso(@RequestBody CNPJ cnpj) {
        registerService.enviarCNPJ(cnpj);
    }
}