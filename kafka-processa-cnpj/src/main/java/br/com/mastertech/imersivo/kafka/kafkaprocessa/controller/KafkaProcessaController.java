package br.com.mastertech.imersivo.kafka.kafkaprocessa.controller;

import br.com.mastertech.imersivo.kafka.model.CNPJ;
import br.com.mastertech.imersivo.kafka.model.DadosCNPJ;
import br.com.mastertech.imersivo.kafka.kafkaprocessa.service.KafkaProcessaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class KafkaProcessaController {

    private static final String ANALISE_ACEITA = "ACEITA";
    private static final String ANALISE_RECUSADA = "RECUSADA";

    @Autowired
    KafkaProcessaService processaService;

    @KafkaListener(topics = "leandro-biro-1", groupId = "leandro-biro-1")
    public void recebeRegistro(@Payload CNPJ cnpj) {
        DadosCNPJ dadosCNPJ = processaService.buscaDadosCpnj(cnpj);
        if (dadosCNPJ != null) {
            dadosCNPJ = processaCapitalCNPJ(dadosCNPJ);
            processaService.enviarDadosCNPJ(dadosCNPJ);
        }
    }

    private DadosCNPJ processaCapitalCNPJ(DadosCNPJ dadosCNPJ) {
        double capital = Double.parseDouble(dadosCNPJ.getCapitalSocial());
        if (capital > 1000000) {
            dadosCNPJ.setStatusAnalise(ANALISE_ACEITA);
        } else {
            dadosCNPJ.setStatusAnalise(ANALISE_RECUSADA);
        }
        return dadosCNPJ;
    }

}
