package br.com.mastertech.imersivo.kafka.kafkaprocessa.client.dto;

import java.util.ArrayList;

public class CnpjDto {

    ArrayList<Object> atividade_principal = new ArrayList<Object>();
    private String data_situacao;
    private String complemento;
    private String nome;
    private String uf;
    private String telefone;
    ArrayList<Object> atividades_secundarias = new ArrayList<Object>();
    ArrayList<Object> qsa = new ArrayList<Object>();
    private String situacao;
    private String bairro;
    private String logradouro;
    private String numero;
    private String cep;
    private String municipio;
    private String porte;
    private String abertura;
    private String natureza_juridica;
    private String cnpj;
    private String ultima_atualizacao;
    private String status;
    private String tipo;
    private String fantasia;
    private String email;
    private String efr;
    private String motivo_situacao;
    private String situacao_especial;
    private String data_situacao_especial;
    private String capital_social;
    Extra ExtraObject;
    Billing BillingObject;


    // Getter Methods

    public String getData_situacao() {
        return data_situacao;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getNome() {
        return nome;
    }

    public String getUf() {
        return uf;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getSituacao() {
        return situacao;
    }

    public String getBairro() {
        return bairro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public String getCep() {
        return cep;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getPorte() {
        return porte;
    }

    public String getAbertura() {
        return abertura;
    }

    public String getNatureza_juridica() {
        return natureza_juridica;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getUltima_atualizacao() {
        return ultima_atualizacao;
    }

    public String getStatus() {
        return status;
    }

    public String getTipo() {
        return tipo;
    }

    public String getFantasia() {
        return fantasia;
    }

    public String getEmail() {
        return email;
    }

    public String getEfr() {
        return efr;
    }

    public String getMotivo_situacao() {
        return motivo_situacao;
    }

    public String getSituacao_especial() {
        return situacao_especial;
    }

    public String getData_situacao_especial() {
        return data_situacao_especial;
    }

    public String getCapital_social() {
        return capital_social;
    }

    public Extra getExtra() {
        return ExtraObject;
    }

    public Billing getBilling() {
        return BillingObject;
    }

    // Setter Methods

    public void setData_situacao( String data_situacao ) {
        this.data_situacao = data_situacao;
    }

    public void setComplemento( String complemento ) {
        this.complemento = complemento;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public void setUf( String uf ) {
        this.uf = uf;
    }

    public void setTelefone( String telefone ) {
        this.telefone = telefone;
    }

    public void setSituacao( String situacao ) {
        this.situacao = situacao;
    }

    public void setBairro( String bairro ) {
        this.bairro = bairro;
    }

    public void setLogradouro( String logradouro ) {
        this.logradouro = logradouro;
    }

    public void setNumero( String numero ) {
        this.numero = numero;
    }

    public void setCep( String cep ) {
        this.cep = cep;
    }

    public void setMunicipio( String municipio ) {
        this.municipio = municipio;
    }

    public void setPorte( String porte ) {
        this.porte = porte;
    }

    public void setAbertura( String abertura ) {
        this.abertura = abertura;
    }

    public void setNatureza_juridica( String natureza_juridica ) {
        this.natureza_juridica = natureza_juridica;
    }

    public void setCnpj( String cnpj ) {
        this.cnpj = cnpj;
    }

    public void setUltima_atualizacao( String ultima_atualizacao ) {
        this.ultima_atualizacao = ultima_atualizacao;
    }

    public void setStatus( String status ) {
        this.status = status;
    }

    public void setTipo( String tipo ) {
        this.tipo = tipo;
    }

    public void setFantasia( String fantasia ) {
        this.fantasia = fantasia;
    }

    public void setEmail( String email ) {
        this.email = email;
    }

    public void setEfr( String efr ) {
        this.efr = efr;
    }

    public void setMotivo_situacao( String motivo_situacao ) {
        this.motivo_situacao = motivo_situacao;
    }

    public void setSituacao_especial( String situacao_especial ) {
        this.situacao_especial = situacao_especial;
    }

    public void setData_situacao_especial( String data_situacao_especial ) {
        this.data_situacao_especial = data_situacao_especial;
    }

    public void setCapital_social( String capital_social ) {
        this.capital_social = capital_social;
    }

    public void setExtra( Extra extraObject ) {
        this.ExtraObject = extraObject;
    }

    public void setBilling( Billing billingObject ) {
        this.BillingObject = billingObject;
    }
}

class Billing {
    private boolean free;
    private boolean database;


    // Getter Methods

    public boolean getFree() {
        return free;
    }

    public boolean getDatabase() {
        return database;
    }

    // Setter Methods

    public void setFree( boolean free ) {
        this.free = free;
    }

    public void setDatabase( boolean database ) {
        this.database = database;
    }
}

class Extra {


    // Getter Methods



    // Setter Methods


}