package br.com.mastertech.imersivo.kafka.model;

public class CNPJ {
    private String cnpj;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}