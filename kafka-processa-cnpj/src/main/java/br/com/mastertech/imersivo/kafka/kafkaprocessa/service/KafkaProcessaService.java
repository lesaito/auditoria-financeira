package br.com.mastertech.imersivo.kafka.kafkaprocessa.service;

import br.com.mastertech.imersivo.kafka.kafkaprocessa.client.CnpjClient;
import br.com.mastertech.imersivo.kafka.kafkaprocessa.client.dto.CnpjDto;
import br.com.mastertech.imersivo.kafka.model.CNPJ;
import br.com.mastertech.imersivo.kafka.model.DadosCNPJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProcessaService {

    @Autowired
    CnpjClient cnpjClient;

    @Autowired//       (tipo chave, tipo valor)
    private KafkaTemplate<String, DadosCNPJ> sender;

    public DadosCNPJ buscaDadosCpnj(CNPJ cnpj) {
        CnpjDto cnpjDto = null;
        try {
            cnpjDto = cnpjClient.buscaDadosCnpj(cnpj.getCnpj());
        } catch (Exception e) {
            e.printStackTrace();
        }
        DadosCNPJ dadosCNPJ = new DadosCNPJ();
        if (cnpjDto != null) {
            dadosCNPJ.setNome(cnpjDto.getNome());
            dadosCNPJ.setTelefone(cnpjDto.getTelefone());
            dadosCNPJ.setSituacao(cnpjDto.getSituacao());
            dadosCNPJ.setAbertura(cnpjDto.getAbertura());
            dadosCNPJ.setNaturezaJuridica(cnpjDto.getNatureza_juridica());
            dadosCNPJ.setCnpj(cnpjDto.getCnpj());
            dadosCNPJ.setStatus(cnpjDto.getStatus());
            dadosCNPJ.setTipo(cnpjDto.getTipo());
            dadosCNPJ.setCapitalSocial(cnpjDto.getCapital_social());
        }
        return dadosCNPJ;
    }

    public void enviarDadosCNPJ(DadosCNPJ dadosCNPJ) {
        //         (topico, chave, valor)
        sender.send("leandro-biro-2", "1", dadosCNPJ);
    }
}
