package br.com.mastertech.imersivo.kafka.kafkaprocessa.client;

import br.com.mastertech.imersivo.kafka.kafkaprocessa.client.dto.CnpjDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cpnjclient", url = "http://www.receitaws.com.br/v1/cnpj")
public interface CnpjClient {

    @GetMapping(value={"/{cnpj}"} )
    public CnpjDto buscaDadosCnpj(@PathVariable (value = "cnpj") String cnpj);
}
